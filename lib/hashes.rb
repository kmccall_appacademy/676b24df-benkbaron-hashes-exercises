# EASY

# Define a method that, given a sentence, returns a hash of each of the words as
# keys with their lengths as values. Assume the argument lacks punctuation.
def word_lengths(str)
  word_length_hash = Hash.new
  words = str.split
  words.each {|word| word_length_hash[word] = word.length}
  word_length_hash
end

# Define a method that, given a hash with integers as values, returns the key
# with the largest value.
def greatest_key_by_val(hash)
  hash.sort_by {|k, v| v}[-1][0]
end

# Define a method that accepts two hashes as arguments: an older inventory and a
# newer one. The method should update keys in the older inventory with values
# from the newer one as well as add new key-value pairs to the older inventory.
# The method should return the older inventory as a result. march = {rubies: 10,
# emeralds: 14, diamonds: 2} april = {emeralds: 27, moonstones: 5}
# update_inventory(march, april) => {rubies: 10, emeralds: 27, diamonds: 2,
# moonstones: 5}
def update_inventory(older, newer)
  newer.each do |k, v|
    older[k] = v
  end
  older
end

# Define a method that, given a word, returns a hash with the letters in the
# word as keys and the frequencies of the letters as values.
def letter_counts(word)
  ltr_hsh = Hash.new(0)
  word.each_char {|ltr| ltr_hsh[ltr] += 1}
  ltr_hsh
end

# MEDIUM

# Define a method that, given an array, returns that array without duplicates.
# Use a hash! Don't use the uniq method.
def my_uniq(arr)
  hsh = Hash.new
  arr.each {|el| hsh[el] = nil}
  hsh.keys
end

# Define a method that, given an array of numbers, returns a hash with "even"
# and "odd" as keys and the frequency of each parity as values.
def evens_and_odds(numbers)
  even_odd_hsh = Hash.new(0)
  numbers.each do |num|
    if num.even?
      even_odd_hsh[:even] += 1
    else
      even_odd_hsh[:odd] += 1
    end
  end
  even_odd_hsh
end

# Define a method that, given a string, returns the most common vowel. If
# there's a tie, return the vowel that occurs earlier in the alphabet. Assume
# all letters are lower case.
def most_common_vowel(string)
  ltr_hsh = Hash.new(0)
  string.each_char do |ltr|
    ltr_hsh[ltr] += 1
  end
  vowels = "aeiou"
  most_frequent = nil
  vowels.each_char do |ltr|
    if most_frequent == nil && ltr_hsh[ltr] > 0
      most_frequent = ltr
    elsif
      ltr_hsh[ltr] > ltr_hsh[most_frequent]
      most_frequent = ltr
    end
  end
  most_frequent
end

# HARD

# Define a method that, given a hash with keys as student names and values as
# their birthday months (numerically, e.g., 1 corresponds to January), returns
# every combination of students whose birthdays fall in the second half of the
# year (months 7-12). students_with_birthdays = { "Asher" => 6, "Bertie" => 11,
# "Dottie" => 8, "Warren" => 9 }
# fall_and_winter_birthdays(students_with_birthdays) => [ ["Bertie", "Dottie"],
# ["Bertie", "Warren"], ["Dottie", "Warren"] ]
def fall_and_winter_birthdays(students)
  second_half_birthdays = students.select {|name, month| month >= 7}
  second_half_names = second_half_birthdays.keys

  name_combos = []
  second_half_names.each_with_index do |name, idx|
    while idx < second_half_names.length - 1
      name_combos << [name, second_half_names[idx+1]]
      idx+=1
    end
  end
  name_combos
end

# Define a method that, given an array of specimens, returns the biodiversity
# index as defined by the following formula: number_of_species**2 *
# smallest_population_size / largest_population_size biodiversity_index(["cat",
# "cat", "cat"]) => 1 biodiversity_index(["cat", "leopard-spotted ferret",
# "dog"]) => 9
def biodiversity_index(specimens)
  species_hsh = Hash.new(0)
  specimens.each {|specimen| species_hsh[specimen] += 1}
  species_num = species_hsh.keys.length
  smallest_pop_num = species_hsh.sort_by {|specimen, pop| pop}[0][1]
  largest_pop_num = species_hsh.sort_by {|specimen, pop| pop}[-1][1]

  species_num**2 * smallest_pop_num / largest_pop_num
end

# Define a method that, given the string of a respectable business sign, returns
# a boolean indicating whether pranksters can make a given vandalized string
# using the available letters. Ignore capitalization and punctuation.
# can_tweak_sign("We're having a yellow ferret sale for a good cause over at the
# pet shop!", "Leopard ferrets forever yo") => true
def can_tweak_sign?(normal_sign, vandalized_sign)
  normal_hsh = character_count(normal_sign)
  vandalized_hsh = character_count(vandalized_sign)

  vandalized_hsh.each {|ltr, count| return false if count > normal_hsh[ltr]}

  true
end

def character_count(str)
  hsh = Hash.new(0)
  str = str.delete(".,'?!")
  str = str.downcase
  str.each_char do |ltr|
    hsh[ltr]+=1
  end
  hsh
end
